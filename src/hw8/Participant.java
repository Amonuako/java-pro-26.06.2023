package hw8;

public abstract class Participant {
    protected String name;
    protected int runLimit;
    protected int jumpLimit;
    protected int stamina;
    protected boolean success;

    public Participant(String name, int runLimit, int jumpLimit,int stamina) {
        this.name = name;
        this.runLimit = runLimit;
        this.jumpLimit = jumpLimit;
        this.stamina = stamina;
        this.success =true;
    }

    public void jump(int height) {
        decreaseStamina(height);
        if (success){
            if(height <= jumpLimit) {
            System.out.println("Участник " + name + " перепрыгнул стену высотой " + height + "м");
        } else {
            System.out.println("Участник " + name + " не смог перепрыгнуть стену высотой " + height
                    + "м,"+" он выбывает");
            System.out.println();
            success =false;


        }
    }
    }
    public void run(int distance) {
         double staminaCost =distance*0.1;
        decreaseStamina(Math.ceil(staminaCost));
            if (success){
                if (distance <= runLimit) {
                    System.out.print("Участник " + name + " пробежал дорожку длиной " + distance + " м. ");

                } else {
                    System.out.println("Участник " + name + " не смог пробежать дорожку длиной " + distance
                            + " м."+" он выбывает");
                    System.out.println();

                    success =false;
                }
            }
        }
    protected void decreaseStamina(double amount) {
        stamina -= amount;

        if (stamina <0) {
            System.out.println("Участнику " + name + " не хватило сил преодолеть препятствие , он выбывает");

            success = false;

            System.out.println();
        }
    }


}








