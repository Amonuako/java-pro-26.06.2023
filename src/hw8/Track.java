package hw8;

public class   Track implements Obstacle {
    private int distance;


    public Track(int distance) {
        this.distance = distance;
    }


    @Override
    public void overcome(Participant participant) {
        participant.run(distance);
        if (participant.success)
            draw();

    }
    public void draw() {
        double lineDistance =distance/10;

        for (int i = 0; i <lineDistance; i++) {
            System.out.print("-");
        }
        System.out.println();

    }
}
