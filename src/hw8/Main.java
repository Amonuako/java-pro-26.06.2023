package hw8;

public class Main {
    public static void main(String[] args) {

        Obstacle[] obstacles = new Obstacle[10];

        int startTrack = 100;
        int startWall = 2;

        for (int i = 0; i < 10; i++) {
            obstacles[i] = (i % 2 == 0) ? new Track(startTrack += 100) : new Wall(startWall += 1);
        }

        Participant[] participant = {
                new Human("Human", 400, 5, 20),
                new Cat("Cat", 300, 3, 100),
                new Robot("Robot", 300, 7, 300)
        };


        for (Participant participants:participant) {
            for (Obstacle obstacle : obstacles) {
                if (participants.success) {
                    obstacle.overcome(participants);
            }
        }
        }

}
}


