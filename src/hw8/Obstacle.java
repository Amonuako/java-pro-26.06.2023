package hw8;

public interface Obstacle {
    void overcome(Participant participant);
    void draw();

}
