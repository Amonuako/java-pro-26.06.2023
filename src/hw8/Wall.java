package hw8;



public class Wall implements Obstacle {
 public int height;

    public Wall(int height) {
        this.height = height;
    }


    @Override
    public void overcome(Participant participant) {
        participant.jump(height);
        if (participant.success)
            draw();

    }
    public void draw() {
        for (int i = 0; i < height; i++) {
            System.out.printf("%" + (50) + "s%n","|_|");
        }

    }
}
