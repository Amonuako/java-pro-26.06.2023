package hw9;
public class Box < T extends Fruit>{
    private final Object[] fruitsArray;
    private int counter;

    public Box(int size) {
        this.fruitsArray = new Object[size];

    }


    public boolean addFruit(T fruit) {
        if (fruit == null) {
            System.out.println("Вы добавили null,добавьте фрукт.");
            return false;
        }
        if (counter< fruitsArray.length){
        fruitsArray[counter++]=fruit;
    }
        else {
            System.out.println("Не хватает места в коробке,поместилось фруктов: "+fruitsArray.length);
        }
        return false;

    }
    public boolean addFruits(T...fruits) {
        final boolean check = fruits.length > fruitsArray.length;
        int i = fruitsArray.length - counter;
        final boolean check2 = i < fruits.length;
        if (!(check || check2)){
            for (T fruit : fruits) {
                addFruit(fruit);
            }
        return true;
    }
        System.out.println("Не хватает места в коробке,поместилось фруктов: "+fruitsArray.length);
        return false;
    }
    public float getWeight() {
        float totalWeight = 0.0f;
        for (Object fruit : fruitsArray) {
            if (fruit instanceof Apple) {
                totalWeight += 1.0f;
            } else if (fruit instanceof Orange) {
                totalWeight += 1.5f;
            }
        }
        return totalWeight;
    }
    public boolean compare(Box<?> anotherBox) {
        if(this.getWeight()==anotherBox.getWeight()){
            return true;
        }
        else return false;
    }
    public void merge(Box<T> anotherBox) {
        if (this == anotherBox) {
            System.out.println("Нельзя сложить коробку саму с собой.");
            return;
        }
        int spaceLeft = fruitsArray.length - counter;
        if (spaceLeft >= anotherBox.counter) {
            for (int i = 0; i < anotherBox.counter; i++) {
                fruitsArray[counter + i] = anotherBox.fruitsArray[i];
            }
            counter += anotherBox.counter;
            anotherBox.counter = 0;
            spaceLeft = fruitsArray.length - counter;
            System.out.println("Слияние удалось,осталось свободного места: " + spaceLeft);
        } else {
            System.out.println("Не хватает места в коробке, поместилось фруктов: " + spaceLeft);
        }
    }
    }

