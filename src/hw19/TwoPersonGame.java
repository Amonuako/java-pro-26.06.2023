package hw19;

public abstract class TwoPersonGame extends Game {

    protected TwoPersonGame() {
        super(2);
    }
}
