package hw19;

public abstract class Game {
    private final int playerAmount;

    protected Game(int playerAmount) {
        this.playerAmount = playerAmount;
    }

    protected abstract void initializeGame();
    protected abstract void playGame();
    protected abstract void endGame();
    protected abstract void printWinner();
    public final void startGame(){
        initializeGame();
        playGame();
        endGame();
    }


}

