package hw19;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TikTakToe extends TwoPersonGame{
    private char[][] board;
    private char currentPlayer;
    private boolean gameOver;

    public TikTakToe() {
        board = new char[3][3];
        currentPlayer = '1';
        gameOver = false;
    }

    @Override
    protected void initializeGame() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = ' ';
            }
        }
        printBoard();
    }

    @Override
    protected void playGame() {
        Scanner scanner = new Scanner(System.in);

        while (!gameOver) {
            System.out.println("Игрок " + currentPlayer + ", введите номер строки (0-2) и номер столбца (0-2): ");
            try {

            int row = scanner.nextInt();
            int col = scanner.nextInt();

            if (isValidMove(row, col)) {
                board[row][col] = (currentPlayer == '1') ? 'X' : 'O';
                printBoard();
                if (checkWin()) {
                    gameOver = true;
                } else if (isBoardFull()) {
                    System.out.println("Ничья");
                    gameOver = true;
                } else {
                    currentPlayer = (currentPlayer == '1') ? '2' : '1';
                }
            } else {
                System.out.println("Некорректный ход. Попробуйте еще раз.");
            }
        }catch (InputMismatchException e) {
                System.out.println("Ошибка ввода. Введите целые числа от 0 до 2");
                scanner.next();
            }
        }
    }

    @Override
    protected void endGame() {
        if (!isBoardFull()) {
            System.out.println("Игровое поле с выигрышной линией:");
            printRedLine();
            printWinner();
        }
    }

    @Override
    protected void printWinner() {
            System.out.println("Поздравляем игрока " + currentPlayer + " с победой!");

    }

    private void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    private void printRedLine() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                char symbol = board[row][col];
                if (symbol == 'X' && currentPlayer == '1' && checkWin()) {
                    System.out.print("\u001B[31m" + symbol + "\u001B[0m | ");
                } else if (symbol == 'O' && currentPlayer == '2' && checkWin()) {
                    System.out.print("\u001B[31m" + symbol + "\u001B[0m | ");
                } else {
                    System.out.print(symbol + " | ");
                }
            }
            System.out.println("\n-------------");
        }
    }

    private boolean isValidMove(int row, int col) {
        return !gameOver && row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == ' ';
    }

    private boolean checkWin() {
        char symbol = (currentPlayer == '1') ? 'X' : 'O';

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == symbol && board[i][1] == symbol && board[i][2] == symbol) {
                return true;
            }
            if (board[0][i] == symbol && board[1][i] == symbol && board[2][i] == symbol) {
                return true;
            }
        }

        if (board[0][0] == symbol && board[1][1] == symbol && board[2][2] == symbol) {
            return true;
        }
        if (board[0][2] == symbol && board[1][1] == symbol && board[2][0] == symbol) {
            return true;
        }

        return false;
    }

    private boolean isBoardFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }
}
