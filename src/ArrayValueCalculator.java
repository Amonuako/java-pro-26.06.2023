public class ArrayValueCalculator {
    public  int doCalc(String[][] array) throws ArraySizeException, ArrayDataException {
        validateArraySize(array);
        int totalSum = 0;
        for (int i = 0; i < array.length; i++) {
            int colSize = array[i].length;

            for (int j = 0; j < colSize; j++) {
                String value = array[i][j];
                if (value == null)
                    throw new ArrayDataException("Неверные данные, ячейка: " + i + ", " + j + " равна null");
                try {
                    int num = Integer.parseInt(array[i][j]);
                    totalSum += num;
                } catch (NumberFormatException e) {
                    throw new ArrayDataException("Неверные данные, ячейка: " + i + ", " + j+" -- введите целое число");
                }
            }
        }

        return totalSum;
    }

    private void validateArraySize(String[][] array) throws ArraySizeException {
        int lineSize = array.length;
        if (lineSize != 4) {
            throw new ArraySizeException("Неверный размер массива.Допустимо 4 строки");
        }

        for (int i = 0; i < lineSize; i++) {
            int colSize = array[i].length;


            if (colSize != 4) {
                throw new ArraySizeException("Неверный размер массива.Допустимо 4 столбца");
            }

        }
    }
}

