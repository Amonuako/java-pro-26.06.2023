

public class Main {
    public static void main(String[] args) {
        String[][] array = {
                {"1", "2", "3", "4"},
                {"5", "6", "7", "8"},
                {"9", "10", "11", "12"},
                {"13", "14", "15", " "},


        };

        ArrayValueCalculator arrayValueCalculator = new ArrayValueCalculator();

        try {
           int result= arrayValueCalculator.doCalc(array);
            System.out.println(result);
        }
         catch (ArrayDataException | ArraySizeException e) {
             System.out.println(e.getMessage());
        }

    }
}