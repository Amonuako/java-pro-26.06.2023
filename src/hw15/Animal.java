package hw15;

public enum Animal {

    LION(4),
    TIGER(4),
    ELEPHANT(4),
    GIRAFFE(4),
    ZEBRA(4),
    DOG(4),
    CAT(4),
    PARROT(2),
    HAMSTER(4),
    RABBIT(4);

    private final int numberOfLegs;

    Animal(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }
}
