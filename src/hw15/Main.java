package hw15;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Animal> wildAnimals = Arrays.asList(Animal.LION,Animal.ZEBRA,Animal.GIRAFFE,Animal.TIGER,Animal.ELEPHANT);

        List<Animal> pets = Arrays.asList(Animal.CAT,Animal.DOG,Animal.PARROT,Animal.HAMSTER,Animal.RABBIT);



        AnimalManipulation animalManipulation = new AnimalManipulation(wildAnimals,pets);

        System.out.println(animalManipulation.AnimalWithMostLegs());
        System.out.println(animalManipulation.createRandomAnimalList(100));
        System.out.println(animalManipulation.TotalLegs(animalManipulation.createRandomAnimalList(100)));
        System.out.println(animalManipulation.groupAnimalsByLegs(animalManipulation.createRandomAnimalList(20)));
        System.out.println(animalManipulation.AnimalsEachSpecies(animalManipulation.createRandomAnimalList(20)));
        System.out.println(animalManipulation.countSpecies(wildAnimals));

    }
}





