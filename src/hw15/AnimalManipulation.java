package hw15;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnimalManipulation {
    private final List<Animal> wildAnimals;
    private final List<Animal> pets;

    public AnimalManipulation(List<Animal> wildAnimals, List<Animal> pets) {
        this.wildAnimals = wildAnimals;
        this.pets = pets;
    }


    public List<Animal> AnimalWithMostLegs() {
        List<Animal> allAnimals = Stream.concat(wildAnimals.stream(), pets.stream())
                .toList();
        int maxLegs = allAnimals.stream()
                .mapToInt(Animal::getNumberOfLegs)
                .max()
                .orElse(0);

        return allAnimals.stream()
                .filter(animal -> animal.getNumberOfLegs() == maxLegs)
                .collect(Collectors.toList());
    }
    public List<Animal> createRandomAnimalList(int count) {
        Random random = new Random();

        return random.ints(count, 0, 2)
                .mapToObj(listChoice -> (listChoice == 0 ? wildAnimals : pets))
                .map(chosenList -> chosenList.get(random.nextInt(chosenList.size())))
                .collect(Collectors.toList());
    }
    public int TotalLegs(List<Animal> animals) {
        return animals.stream()
                .mapToInt(Animal::getNumberOfLegs)
                .sum();
    }
    public Map<Integer, List<Animal>> groupAnimalsByLegs(List<Animal> animals) {
        return animals.stream()
                .collect(Collectors.groupingBy(Animal::getNumberOfLegs));
    }
    public Map<Animal, Long> AnimalsEachSpecies(List<Animal> animals) {
        return animals.stream()
                .collect(Collectors.groupingBy(animal -> animal, Collectors.counting()));
    }
    public long countSpecies(List<Animal> animals) {
        return animals.stream()
                .distinct()
                .count();
    }

}
