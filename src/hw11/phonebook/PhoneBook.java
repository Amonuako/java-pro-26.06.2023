package hw11.phonebook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneBook {
    private Map<String, List<Record>> recordsMap = new HashMap<>();

    public void add(Record record) {
        List<Record> records = recordsMap.getOrDefault(record.getName(), new ArrayList<>());
        records.add(record);
        recordsMap.put(record.getName(), records);
    }

    public Record find(String name) {
        List<Record> records = recordsMap.get(name);
        if (records != null && !records.isEmpty()) {
            Record foundRecord = records.get(0);
            System.out.println("Найдена запись -- Имя: " + foundRecord.getName() + ", Телефон: " + foundRecord.getPhoneNumber());
            return foundRecord;
        } else {
            System.out.println("Запись с именем " + name + " не найдена.");
            return null;
        }
    }

    public List<Record> findAll(String name) {
        List<Record> records = recordsMap.get(name);
        if (records != null && !records.isEmpty()) {
            System.out.println("Найдены записи:");
            for (Record record : records) {
                System.out.println("Имя: " + record.getName() + ", Телефон: " + record.getPhoneNumber());
            }
            return records;
        } else {
            System.out.println("Записи с именем " + name + " не найдены.");
            return null;
        }
    }
}

