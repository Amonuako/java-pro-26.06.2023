public class Pizza {
    private final vegetablesType vegetables;
    private final sauceType sauce;
    private final meatType meat;
    private final cheeseType cheese;

    public Pizza(vegetablesType vegetables, sauceType sauce, meatType meat, cheeseType cheese) {
        this.vegetables = vegetables;
        this.sauce = sauce;
        this.meat = meat;
        this.cheese = cheese;
    }



    public enum vegetablesType{
        olives,tomato,mushroom
    }
    public enum sauceType{
        chilli,garlic,BBQ
    }
    public enum meatType{
        bacon,chicken,veal
    }
    public enum cheeseType{
        cheddar,mozzarella,feta

    }

    @Override
    public String toString() {
        return "Pizza{" +
                "vegetables=" + vegetables +
                ", sauce=" + sauce +
                ", meat=" + meat +
                ", cheese=" + cheese +
                '}';
    }
public static pizzaBuilder builder(){
        return new pizzaBuilder();
}

    public static class pizzaBuilder {
        private vegetablesType vegetables;
        private sauceType sauce;
        private meatType meat;
        private cheeseType cheese;

         private pizzaBuilder() {
        }

        public Pizza build(){
            return new Pizza(this.vegetables,this.sauce,this.meat,this.cheese);
        }

        public Pizza.pizzaBuilder Vegetables(vegetablesType vegetables) {
            this.vegetables = vegetables;
            return this;
        }

        public Pizza.pizzaBuilder Sauce(sauceType sauce) {
            this.sauce = sauce;
            return this;
        }

        public Pizza.pizzaBuilder Meat(meatType meat) {
            this.meat = meat;
            return this;
        }

        public Pizza.pizzaBuilder Cheese(cheeseType cheese) {
            this.cheese = cheese;
            return this;
        }
    }

}
