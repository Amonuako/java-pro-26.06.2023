package hw14;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static hw14.ProductType.*;

public class App {
    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>(Arrays.asList(
                new Product(Comics,10,false, LocalDate.of(2023,3,12),1),
                new Product(Manga,15,false, LocalDate.of(2022,3,13),2),
                new Product(Book,300,true, LocalDate.of(2023,3,14),3),
                new Product(Book,350,true, LocalDate.of(2023,3,15),4)
        ));
        List<Product> find250 = ProductOperation.expensiveBooks(productList);

        List<Product> discount = ProductOperation.discountBooks(productList);

        Product cheap = ProductOperation.findCheapestBook(productList);

        List<Product> lastAdd = ProductOperation.getLastThreeAddedProducts(productList);

        double calc = ProductOperation.calculateTotalCost(productList);
        System.out.println(calc);

        Map<ProductType,List<Product>> group = ProductOperation.groupProductsByType(productList);
        System.out.println(group);



    }
}
