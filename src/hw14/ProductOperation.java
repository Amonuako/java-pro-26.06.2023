package hw14;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductOperation {
    public static List<Product> expensiveBooks(List<Product> productList) {
        List<Product>expensiveBooks= productList.stream()
                .filter(product -> product.getType()==ProductType.Book && product.getPrice() > 250)
                .collect(Collectors.toList());
        System.out.println("Found expensive books: ");
        expensiveBooks.forEach(product -> System.out.println("Type: " + product.getType() + ", Price: " + product.getPrice()));

        return expensiveBooks;

}
    public static List<Product> discountBooks(List<Product> productList) {
         List<Product> discountedBooks = productList.stream()
                .filter(product -> product.getType()==ProductType.Book && product.isDiscount())
                .map(product -> {
                            Product copy = new Product(product.getType(), product.getPrice(), product.isDiscount(), product.getCreateDate(), product.getId());
                            copy.setPrice(copy.getPrice() * 0.9);
                            return copy;
                }
                )
                .collect(Collectors.toList());
        System.out.println("Books at a discount:");
        discountedBooks.forEach(product -> System.out.println("Type: " + product.getType() + ", Price after discount: " + product.getPrice()));

        return discountedBooks;
    }
    public static Product findCheapestBook(List<Product> productList) {
        Optional<Product> cheapestBook = productList.stream()
                .filter(product -> product.getType()==ProductType.Book)
                .min(Comparator.comparingDouble(Product::getPrice));
        if (cheapestBook.isPresent()) {
            Product cheapest = cheapestBook.get();
            System.out.println("Cheapest book: " +  " Price - " + cheapest.getPrice());
            return cheapestBook.get();
        } else {
            throw new RuntimeException("Product not found");
        }
    }
    public static List<Product> getLastThreeAddedProducts(List<Product> productList) {
        List<Product> lastThreeAdded = productList.stream()
                .sorted(Comparator.comparing(Product::getCreateDate).reversed())
                .limit(3)
                .collect(Collectors.toList());
        System.out.println("Last three added products:");
        lastThreeAdded.forEach(product -> System.out.println("Type: " + product.getType() + ", Date: " + product.getCreateDate()));

        return lastThreeAdded;
    }
    public static double calculateTotalCost(List<Product> productList) {
        LocalDate currentDate = LocalDate.now();
        return productList.stream()
                .filter(product -> product.getCreateDate().getYear() == currentDate.getYear())
                .filter(product -> product.getType()==ProductType.Book)
                .filter(product -> product.getPrice() <= 75)
                .mapToDouble(Product::getPrice)
                .sum();
    }
    public static Map<ProductType, List<Product>> groupProductsByType(List<Product> productList) {
        return productList.stream()
                .collect(Collectors.groupingBy(Product::getType));
    }

}
