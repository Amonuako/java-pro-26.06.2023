package hw14;

import java.time.LocalDate;

public class Product {
//    private String type;
    private double price;
    private boolean discount;
    private LocalDate createDate;
    private int id;
    private ProductType type;

    public Product(ProductType type,double price, boolean discount, LocalDate createDate, int id ) {
        this.price = price;
        this.discount = discount;
        this.createDate = createDate;
        this.id = id;
        this.type = type;
    }

        public ProductType getType() {
        return type;
    }


    public double getPrice() {
        return price;
    }

    public boolean isDiscount() {
        return discount;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public int getId() {
        return id;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
