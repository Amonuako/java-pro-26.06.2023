public  class Employee {
    private String name;
    private String jobTitle;
    private String email;
    private String telNumber;
    private int age;
    public Employee(String name, String jobTitle, String email, String telNumber, int age) {
        this.name = name;
        this.jobTitle = jobTitle;
        this.email = email;
        this.telNumber = telNumber;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getEmail() {
        return email;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
