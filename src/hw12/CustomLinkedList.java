package hw12;

import java.util.*;


public class CustomLinkedList<E> implements List<E> {
    private Node<E> head;
    private Node<E> tail;

    private int size;

    private static class Node<E> {

        E data;
        Node<E> next;
        Node<E> prev;

        Node(E element) {
            data = element;
            next = null;
        }


    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        Node<E> current = head;
        if (o == null) {
            for (; current != null; current = current.next) {
                if (current.data == null) {
                    return true;
                }
            }
        } else {
            for (; current != null; current = current.next) {
                if (o.equals(current.data)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new Iterator<E>() {
            private Node<E> current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                E data = current.data;
                current = current.next;
                return data;
            }
    };
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Node<E> current = head;
        for (int index = 0; index < size; index++) {
            array[index] = current.data;
            current = current.next;
        }
        return array;
    }

    @Override
    public boolean add(E o) {
            Node<E> newNode = new Node<>(o);
            if (head == null) {
                head = newNode;
            } else {
                Node<E> current = head;
                while (current.next != null) {
                    current = current.next;
                }
                current.next = newNode;
            }
            size++;
            return true;
        }


    @Override
    public boolean remove(Object o) {
        Node<E> current = head;
        Node<E> prev = null;

        if (o == null) {
            for (;current != null; prev = current, current = current.next) {
                if (current.data == null) {
                    if (prev == null) {
                        head = current.next;
                    } else {
                        prev.next = current.next;
                    }
                    size--;
                    return true;
                }
            }
        } else {
            for (; current != null; prev = current, current = current.next) {
                if (o.equals(current.data)) {
                    if (prev == null) {
                        head = current.next;
                    } else {
                        prev.next = current.next;
                    }
                    size--;
                    return true;
                }
            }
        }

        return false;
    }


    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean modified = false;
        for (E element : c) {
            add(element);
            modified = true;
        }
        return modified;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        Object[] arr = c.toArray();
        int numNew = arr.length;
        if (numNew == 0) {
            return false;
        }

        Node<E> pred;
        Node<E> succ;
        if (index == size) {
            succ = null;
            pred = null;
        } else {
            succ = head;
            for (int i = 0; i < index; i++) {
                succ = succ.next;
            }
            pred = succ.prev;
        }

        for (Object obj : arr) {
            if (obj != null) {
                E e = (E) obj;
                Node<E> newNode = new Node<>(e);
                if (pred == null) {
                    head = newNode;
                } else {
                    pred.next = newNode;
                }
                pred = newNode;
            } else {
                throw new ClassCastException("Cannot cast object to the required type");
            }
        }

        if (succ == null) {
            head = pred;
        } else {
            succ.prev = pred;
            pred.next = succ;
        }


        size += numNew;
        return true;
    }


    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        Node<E> current = head;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current.data;
    }

    @Override
    public E set(int index, E element) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        Node<E> current = head;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        E oldValue = current.data;
        current.data = (E) element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        if (index == 0) {
            Node<E> newNode =  new Node<>(element);
            newNode.next = head;
            head = newNode;
        } else {
            Node<E> current = head;
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
            }
            Node<E> newNode =  new Node<>(element);
            newNode.next = current.next;
            current.next = newNode;
        }

        size++;

    }

    @Override
    public E remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        Node<E> removedNode;
        if (index == 0) {
            removedNode = head;
            head = head.next;
        } else {
            Node<E> current = head;
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
            }
            removedNode = current.next;
            current.next = current.next.next;
        }

        size--;
        return removedNode.data;
    }

    @Override
    public int indexOf(Object o) {
        Node<E> current = head;
        for (int index = 0; current != null; index++, current = current.next) {
            if (Objects.equals(o, current.data)) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<E> current = head;
        int lastIndex = -1;
        for (int index = 0; current != null; index++, current = current.next) {
            if (Objects.equals(o, current.data)) {
                lastIndex = index;
            }
        }
        return lastIndex;
    }

    @Override
    public ListIterator listIterator() {
        return new ListIterator<E>() {
            private Node<E> current = head;
            private Node<E> lastReturned = null;
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                lastReturned = current;
                current = current.next;
                currentIndex++;
                return lastReturned.data;
            }

            @Override
            public boolean hasPrevious() {
                return currentIndex > 0;
            }

            @Override
            public E previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException();
                }
                if (current == null) {
                    current = tail;
                } else {
                    current = current.prev;
                }
                lastReturned = current;
                currentIndex--;
                return lastReturned.data;
            }

            @Override
            public int nextIndex() {
                return currentIndex;
            }

            @Override
            public int previousIndex() {
                return currentIndex - 1;
            }

            @Override
            public void remove() {
                if (lastReturned == null) {
                    throw new IllegalStateException();
                }
                Node<E> prev = lastReturned.prev;
                Node<E> next = lastReturned.next;
                if (prev == null) {
                    head = next;
                } else {
                    prev.next = next;
                    lastReturned.prev = null;
                }
                if (next == null) {
                    tail = prev;
                } else {
                    next.prev = prev;
                    lastReturned.next = null;
                }
                lastReturned = null;
                size--;
                currentIndex--;
            }

            @Override
            public void set(E e) {
                if (lastReturned == null) {
                    throw new IllegalStateException();
                }
                lastReturned.data = e;
            }

            @Override
            public void add(E e) {
                Node<E> newNode = new Node<>(e);
                if (current == null) {
                    if (tail == null) {
                        head = newNode;
                    } else {
                        newNode.prev = tail;
                        tail.next = newNode;
                    }
                    tail = newNode;
                } else {
                    if (current.prev == null) {
                        newNode.next = current;
                        current.prev = newNode;
                        head = newNode;
                    } else {
                        newNode.prev = current.prev;
                        newNode.next = current;
                        current.prev.next = newNode;
                        current.prev = newNode;
                    }
                }
                lastReturned = null;
                size++;
                currentIndex++;
            }
        };

    }

    @Override
    public ListIterator listIterator(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        ListIterator<E> listIterator = listIterator();
        for (int i = 0; i < index; i++) {
            listIterator.next();
        }
        return listIterator;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex > size || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException("Invalid range: " + fromIndex + " to " + toIndex + ", Size: " + size);
        }

        CustomLinkedList<E> subList = new CustomLinkedList<>();
        Node<E> current = head;
        for (int i = 0; i < fromIndex; i++) {
            current = current.next;
        }
        for (int i = fromIndex; i < toIndex; i++) {
            subList.add(current.data);
            current = current.next;
        }

        return subList;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean modified = false;
        Node<E> current = head;
        while (current != null) {
            if (!c.contains(current.data)) {
                remove(current.data);
                modified = true;
            }
            current = current.next;
        }
        return modified;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean modified = false;
        for (Object o : c) {
            if (remove(o)) {
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (a.length < size) {
            a = Arrays.copyOf(a, size);
        }

        Node<E> current = head;
        for (int i = 0; i < size; i++) {
            a[i] = current.data;
            current = current.next;
        }

        if (a.length > size) {
            a[size] = null;
        }

        return a;
    }
}
