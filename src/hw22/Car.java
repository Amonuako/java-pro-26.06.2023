package hw22;

public enum Car {
    Mustang("Бензин", 60.0),
    Toyota("Бензин", 50.0),
    Audi("Дизель", 40.0),
    Mercedes("Бензин", 55.0),
    Deo("Газ", 45.0);

    private final String fuelType;
    private final double tankCapacity;

    Car(String fuelType, double tankCapacity) {
        this.fuelType= fuelType;
        this.tankCapacity= tankCapacity;
    }

    public String getFuelType() {
        return fuelType;
    }

    public double getTankCapacity() {
        return tankCapacity;
    }
}
