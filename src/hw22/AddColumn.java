package hw22;

public class AddColumn implements Runnable{

    private final FuelStation fuelStation;
    private final String threadName;
    private Thread thread;


    public AddColumn(FuelStation fuelStation, String threadName ) {
        this.fuelStation = fuelStation;
        this.threadName = threadName;
        this.thread = new Thread(this, threadName);
        this.thread.start();
    }

    @Override
    public void run() {
        while (true) {
            Car car = fuelStation.dequeueCar();
            if (car != null) {
                System.out.println(threadName + ": Заправка " + car.name()+ " (Тип пального: " + car.getFuelType() + ", Розмір бака: " + car.getTankCapacity() + " л)");
                System.out.println();
               fuelStation.refuelCar(car);
                System.out.println(threadName + ": Заправка " + car.name() + " завершена.");
                System.out.println();

            } else {
                break;
            }
        }
}
}
