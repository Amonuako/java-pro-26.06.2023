package hw22;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FuelStation {
    public double getGasolineStock() {
        return gasolineStock;
    }

    public double getGasStock() {
        return gasStock;
    }

    public double getDieselStock() {
        return dieselStock;
    }

    double gasolineStock = 5000;
    double gasStock = 5000;
    double dieselStock = 5000;
    final int refuelingColumn = 2;
    private final Semaphore semaphore = new Semaphore(refuelingColumn);
    private List<Car> cars;
    private LinkedList<Car> carQueue = new LinkedList<>();

    public FuelStation(List<Car> cars) {
        this.cars = cars;
        addCarsToQueue(cars);

    }
    public void addCarsToQueue(List<Car> carsToAdd) {
        for (Car car : carsToAdd) {
            if (carQueue.size() < 10) {
                carQueue.add(car);
            } else {
                System.out.println("Очередь машин переполнена, только 10 машин будет заправлено");
                break;
            }
        }
    }
    public Car dequeueCar() {
        Car car;
        synchronized (carQueue) {
            car = carQueue.poll();
        }
        return car;
    }
    public  void refuelCar(Car car) {
        try {
            semaphore.acquire();
            double tankCapacity = car.getTankCapacity();
            double refuelingSpeed = 5.0;
            long refuelingTimeMillis = (long) (tankCapacity / refuelingSpeed * 1000);
            switch (car.getFuelType()) {
                case "Бензин" -> refuelingGasoline(car,refuelingTimeMillis);
                case "Газ" -> refuelingGas(car,refuelingTimeMillis);
                case "Дизель" -> refuelingDiesel(car,refuelingTimeMillis);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            semaphore.release();
        }
    }

    public  void refuelingGasoline(Car car,long refuelingTimeMillis) throws InterruptedException {
        if (gasolineStock >= car.getTankCapacity()) {
            Thread.sleep(refuelingTimeMillis);
            gasolineStock -= car.getTankCapacity();
        } else {
            System.out.println("На заправці немає достатньо бензину для " + car.name());
        }
    }


    public  void refuelingGas(Car car,long refuelingTimeMillis) throws InterruptedException {
        if (gasStock >= car.getTankCapacity()) {
            Thread.sleep(refuelingTimeMillis);
            gasStock -= car.getTankCapacity();
        } else {
            System.out.println("На заправці немає достатньо газу для " + car.name());
        }
    }


    public  void refuelingDiesel(Car car,long refuelingTimeMillis) throws InterruptedException {
        if (dieselStock >= car.getTankCapacity()) {
            Thread.sleep(refuelingTimeMillis);
            dieselStock -= car.getTankCapacity();
        } else {
            System.out.println("На заправці немає достатньо дизеля для " + car.name());
        }
    }
}

