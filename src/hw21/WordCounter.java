package hw21;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class WordCounter {

    public static int countWords(String data) {
        String[] words = data.split("[\\s,]+");
        return words.length;
    }
    public static int countRepeatingWords(String data) {
        String[] words = data.split("[\\s,]+");
        Set<String> uniqueWords = new HashSet<>();
        int repeatingWordCount = 0;

        for (String word : words) {
            if (!uniqueWords.add(word)) {
                repeatingWordCount++;
            }
        }

        return repeatingWordCount;
    }
    public static String findMostRepeatedWord(String data) {
        String[] words = data.split("[\\s,]+");
        Map<String, Integer> wordCountMap = new HashMap<>();

        for (String word : words) {
            wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
        }

        String mostRepeatedWord = "";
        int maxCount = 0;

        for (Map.Entry<String, Integer> entry : wordCountMap.entrySet()) {
            if (entry.getValue() > maxCount) {
                mostRepeatedWord = entry.getKey();
                maxCount = entry.getValue();
            }
        }

        return mostRepeatedWord;
    }
}
