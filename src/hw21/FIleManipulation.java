package hw21;

import java.io.*;
public  class FIleManipulation {
    public static String readInputFile(String fileName) throws IOException {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            boolean containsInt = false;
            while ((line = reader.readLine()) != null) {
            if (line.matches(".*\\d.*")) {
                containsInt = true;
                break;
            }
            content.append(line).append("\n");
        }
            if (containsInt) {
            System.err.println("Ошибка: Файл содержит цифры. Введите только слова.");
            return "";
        }
        return content.toString();
    }
    }

    public static void writeToFile(String fileName, String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(content);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
