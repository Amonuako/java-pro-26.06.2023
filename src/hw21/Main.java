package hw21;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            String inputContent = FIleManipulation.readInputFile("C:\\Users\\Amonu\\IdeaProjects\\hw6.2\\src\\hw21\\input.txt");

            Thread wordCountThread = new Thread(new WriteToFileRunnable("output.txt", "Total Word Count: " + WordCounter.countWords(inputContent)));
            Thread repeatingWordCountThread = new Thread(new WriteToFileRunnable("output.txt", "Repeating Word Count: " + WordCounter.countRepeatingWords(inputContent)));
            Thread mostRepeatedWordThread = new Thread(new WriteToFileRunnable("output.txt", "Most Repeated Word: " + WordCounter.findMostRepeatedWord(inputContent)));

            wordCountThread.start();
            repeatingWordCountThread.start();
            mostRepeatedWordThread.start();

            wordCountThread.join();
            repeatingWordCountThread.join();
            mostRepeatedWordThread.join();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    }

