package hw21;

public class WriteToFileRunnable implements Runnable {
    private final String fileName;
    private final String content;

    public WriteToFileRunnable(String fileName, String content) {
        this.fileName = fileName;
        this.content = content;
    }

    @Override
    public void run() {
        FIleManipulation.writeToFile(fileName, content);
    }
}
