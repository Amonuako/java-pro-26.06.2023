package hw13.coffee.order;

import java.util.ArrayList;
import java.util.List;

public class CoffeeOrderBoard {
    private List<Order> orders;

    public CoffeeOrderBoard() {
        orders = new ArrayList<>();
    }
    public void add(String customerName) {
        int nextOrderNumber = 1;
        if (!orders.isEmpty()) {
            int maxOrderNumber = -1;
            for (Order order : orders) {
                maxOrderNumber = Math.max(maxOrderNumber, order.getOrderNumber());
            }
            nextOrderNumber = maxOrderNumber + 1;
        }

        orders.add(new Order(nextOrderNumber, customerName));
    }
    public void deliver() {
        if (!orders.isEmpty()) {
            Order nextOrder = orders.get(0);
            System.out.println("Delivering order: " + nextOrder);
            orders.remove(0);
        } else {
            System.out.println("No orders to deliver.");
        }
    }
    public void deliver(int orderNumber) {
        Order foundOrder = null;
        for (Order order : orders) {
            if (order.getOrderNumber() == orderNumber) {
                foundOrder = order;
                break;
            }
        }

        if (foundOrder != null) {
            System.out.println("Delivering order: " + foundOrder);
            orders.remove(foundOrder);
        } else {
            System.out.println("Order with number " + orderNumber + " not found.");
        }
    }
    public void draw() {
        System.out.println("=============");
        System.out.println("Num | Name");
        for (Order order : orders) {
            System.out.println(order);
        }
        System.out.println("=============");
    }
    
}
