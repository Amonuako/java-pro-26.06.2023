public class Burger {
    private String bun;
    private String meat;
    private String cheese;
    private String salad;
    private boolean mayo;

    public Burger(String bun, String meat, String cheese, String salad,boolean mayo) {

        this.bun = bun;
        this.meat = meat;
        this.cheese = cheese;
        this.salad = salad;
        this.mayo = mayo;

        burgerComposition();
    }
    public Burger(String bun, String meat, String cheese, String salad) {
        this(bun, meat, cheese, salad,false);
    }
    public Burger(String bun, String meat) {
        this(bun, meat, "чеддер" , "салат",true);
    }






    public void burgerComposition(){
        System.out.println("Состав бургера:");
        System.out.println("Булочка: " + bun);
        System.out.println("Мясо: " + meat);
        System.out.println("Сыр: " + cheese);
        System.out.println("Зелень: " + salad);
        System.out.println("Майонез: " + (mayo ? "да" : "Нет"));
        System.out.println();
        }
}


