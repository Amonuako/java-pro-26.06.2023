package hw20;

import hw20.decorator.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class SerMoveDesFile {
    public static void main(String[] args) {

        String targetPath ;

        try {

            File file = createAndSerializeFile();

            File to = new File("C:\\Users\\Amonu\\Desktop\\test");

            targetPath=moveFile(file, to);

            System.out.println("Файл успішно створено і перенесено.");
            deserialize(targetPath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    private static File createAndSerializeFile() throws IOException {
        File file = new File("temp.txt");

        try (ObjectOutputStream ser = new ObjectOutputStream(new FileOutputStream(file))) {
            Drone drone = new KamikazeDroneDecorator(new CombatDroneDecorator(new ReconnaissanceDroneDecorator(new BasicDrone())));
            ser.writeObject(drone);
            ser.flush();
        }

        return file;
    }

    private static String moveFile(File sourceFile, File targetDirectory) throws IOException {

        Path sourcePath = sourceFile.toPath();
        Path targetPath = Paths.get(targetDirectory.getPath(), sourceFile.getName());

        Files.move(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);

        return targetPath.toString();
    }
    private static void deserialize(String filePath) {
            try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filePath))){

                Drone drone = (Drone) in.readObject();

                System.out.println(drone.performTask());

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
    }
    }




