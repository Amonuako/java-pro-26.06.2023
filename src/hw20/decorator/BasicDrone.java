package hw20.decorator;

public class BasicDrone implements Drone {
    @Override
    public String performTask() {
        return "Створили базовий дрон.";
    }
}
