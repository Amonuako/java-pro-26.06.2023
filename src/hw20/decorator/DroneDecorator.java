package hw20.decorator;

public class DroneDecorator implements Drone {
    Drone drone;

    public DroneDecorator(Drone drone) {
        this.drone = drone;
    }

    @Override
    public String performTask() {
       return drone.performTask();
    }
}
