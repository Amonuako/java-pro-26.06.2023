package hw20.decorator;

import java.io.Serializable;

public interface Drone extends Serializable {
     String performTask();
}
