package hw24.factory;

import hw24.common.Logger;
import hw24.file.FileLogger;

import java.io.IOException;

public class FileLoggerFactory {

    public static Logger getLogger(Class<?> clazz) {
        try {
            return new FileLogger(clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
