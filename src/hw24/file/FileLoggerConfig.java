package hw24.file;

import hw24.common.LoggerConfig;

public class FileLoggerConfig extends LoggerConfig {

    public static final String FILE_EXT = ".log";
    private final String fileName;
    private final String colorLogging;
    private final String infoMessageFormat;
    private final String debugMessageFormat;

    public FileLoggerConfig(String timeFormat,
                            Level level,
                            String fileName, String colorLogging, String infoMessageFormat, String debugMessageFormat) {
        super(timeFormat, level);
        this.fileName = fileName;
        this.colorLogging=colorLogging;
        this.infoMessageFormat = infoMessageFormat;
        this.debugMessageFormat = debugMessageFormat;
    }

    public String getInfoMessageFormat() {
        return infoMessageFormat;
    }

    public String getDebugMessageFormat() {
        return debugMessageFormat;
    }

    public String getColorLogging() {
        return colorLogging;
    }

    public String getFileName() {
        return fileName;
    }
}
