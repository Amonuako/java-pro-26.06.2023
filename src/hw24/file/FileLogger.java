package hw24.file;

import hw24.common.Logger;
import hw24.common.LoggerConfig;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

import static java.text.MessageFormat.format;

public class FileLogger extends Logger {

    private final FileWriter fileWriter;
    private final FileLoggerConfig config;
    private final Class<?> clazz;

    public FileLogger(Class<?> clazz) throws IOException {
        FileLoggerConfigLoader fileLoggerConfigLoader = new FileLoggerConfigLoader();

        this.config = fileLoggerConfigLoader.load("C:\\Users\\Amonu\\IdeaProjects\\hw6.2\\src\\logging.properties");
        this.fileWriter = new FileWriter(config.getFileName().concat(FileLoggerConfig.FILE_EXT), true);
        this.clazz = clazz;
    }

    @Override
    public void info(String data) {
        LoggerConfig.Level level = config.getLevel();
        if (level == LoggerConfig.Level.INFO || level == LoggerConfig.Level.DEBUG)
            write(data, LoggerConfig.Level.INFO);
    }

    @Override
    public void debug(String data) {
        if (config.getLevel() == LoggerConfig.Level.DEBUG)
            write(data, LoggerConfig.Level.DEBUG);
    }


    private void write(String message, LoggerConfig.Level level) {
        try {
            String logMessage = switch (level) {
                case INFO -> format(
                        LoggerConfig.MESSAGE_FORMAT_INFO,
                        level,
                        message);
                case DEBUG -> format(
                        LoggerConfig.MESSAGE_FORMAT_DEBUG,
                        level,
                        clazz.getName(),
                        LocalDateTime.now().format(config.getFormat()),
                        message);
            };

            switch (config.getColorLogging().toLowerCase()) {
                case "green" -> System.out.println("\u001B[32m" + logMessage + "\u001B[0m");
                case "yellow" -> System.out.println("\u001B[33m" + logMessage + "\u001B[0m");
                case "red" -> System.out.println("\u001B[31m" + logMessage + "\u001B[0m");
                default -> System.out.println(logMessage);
            }

            fileWriter.write(System.lineSeparator() + logMessage);
        } catch (IOException e) {
            throw new RuntimeException("Ошибка при записи в файл", e);
        } finally {
            try {
                fileWriter.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
