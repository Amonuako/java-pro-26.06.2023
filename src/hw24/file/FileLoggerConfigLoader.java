package hw24.file;

import hw24.common.LoggerConfig;
import hw24.common.LoggerConfigLoader;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

public class FileLoggerConfigLoader extends LoggerConfigLoader {

    @Override
    public FileLoggerConfig load(String resource) throws IOException {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(resource)) {
            properties.load(reader);

            String loggingLevel = properties.getProperty("level", LoggerConfig.DEFAULT_LEVEL.toString());
            String loggingTimeFormat = properties.getProperty("time.format", LoggerConfig.DEFAULT_FORMAT);
            String loggingFileName = properties.getProperty("fileName", UUID.randomUUID().toString());
            String colorLogging = properties.getProperty("colorLogging", "default");
            String infoMessageFormat = properties.getProperty("message.info.format", LoggerConfig.MESSAGE_FORMAT_INFO);
            String debugMessageFormat = properties.getProperty("message.debug.format", LoggerConfig.MESSAGE_FORMAT_DEBUG);

            return new FileLoggerConfig(loggingTimeFormat, LoggerConfig.Level.valueOf(loggingLevel.toUpperCase()), loggingFileName, colorLogging, infoMessageFormat, debugMessageFormat);
        }
    }
}
