package hw24;

import hw24.common.Logger;
import hw24.factory.FileLoggerFactory;

public class ValueCalculator {
        private final float[] arrNumbers;
        private final int size;
        private final int halfSize;
        private final Logger LOGGER = FileLoggerFactory.getLogger(ValueCalculator.class);

        public ValueCalculator(int size) {
            if (size < 1000000) {
                throw new IllegalArgumentException("Розмір масиву повинен бути не менше 1 000 000");
            }
            this.size = size;
            this.halfSize = size / 2;
            this.arrNumbers = new float[size];
        }

        public void calculateValues() {
            long start = System.currentTimeMillis();

            for (int i = 0; i < size; i++) {
                arrNumbers[i] = 1.0f;
            }


            float[] arr1 = new float[halfSize];
            float[] arr2 = new float[halfSize];

            System.arraycopy(arrNumbers, 0, arr1, 0, halfSize);
            System.arraycopy(arrNumbers, halfSize, arr2, 0, halfSize);


            Thread thread1 = new Thread(() -> calculateArray(arr1, 0));
            Thread thread2 = new Thread(() -> calculateArray(arr2, halfSize));


            thread1.start();
            thread2.start();

            try {
                thread1.join();
                thread2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.arraycopy(arr1, 0, arrNumbers, 0, halfSize);
            System.arraycopy(arr2, 0, arrNumbers, halfSize, halfSize);
            LOGGER.info("Вычисление значений завершено");

            long end = System.currentTimeMillis();
            long totalTime = end - start;

            LOGGER.debug("Час виконання: " + totalTime + " мс");

        }

        private  void calculateArray(float[] subArray, int startIndex) {
            for (int i = 0; i < subArray.length; i++) {
                int index = startIndex + i;
                subArray[i] = (float)(arrNumbers[index] * Math.sin(0.2f + index / 5) *
                        Math.cos(0.2f + index / 5) * Math.cos(0.4f + index / 2));
            }
        }
    }

