package hw24.common;

import java.time.format.DateTimeFormatter;

public abstract class LoggerConfig {

    /**
     * <ol>
     *     <li>
     *         level
     *     </li>
     *     <li>
     *         message
     *     </li>
     *     <li>
     *         class name
     *     </li>
     *     <li>
     *         time
     *     </li>
     * </ol>
     */
    public static final String MESSAGE_FORMAT_DEBUG = "{0} {1} {2} {3}";
    public static final String MESSAGE_FORMAT_INFO = "{0} {1}";
    public static final String DEFAULT_FORMAT = "yyyy.MM.dd hh:mm:ss";
    public static final Level DEFAULT_LEVEL = Level.INFO;
    private final String currentFormat;
    private final Level level;



    protected LoggerConfig(String currentFormat, Level level) {
        this.currentFormat = currentFormat;
        this.level = level;
    }


    public Level getLevel() {
        return level;
    }

    public DateTimeFormatter getFormat() {
        return DateTimeFormatter.ofPattern(currentFormat);
    }

    public enum Level {
        DEBUG,
        INFO
    }


}
