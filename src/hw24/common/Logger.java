package hw24.common;

public abstract class Logger {

    public abstract void info(String data) ;

    public abstract void debug(String data) ;
}
