package hw24.common;

import java.io.IOException;

public abstract class LoggerConfigLoader<T extends LoggerConfig> {

    public abstract T load(String resource) throws IOException;

}
