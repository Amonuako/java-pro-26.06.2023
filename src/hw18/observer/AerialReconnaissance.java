package hw18.observer;

import java.util.ArrayList;
import java.util.List;

public class AerialReconnaissance implements Subject{


    private List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(String alert) {
        for (Observer observer : observers) {
            observer.update(alert);
        }
    }

    public void raiseAlert(String alert) {

        notifyObservers(alert);
    }
    public void clearAlert() {
        notifyObservers("Відбій тривоги");
    }
}
