package hw18.observer;

public class Main {
    public static void main(String[] args) {
        AerialReconnaissance aerialReconnaissance = new AerialReconnaissance();

        People person1 = new People("Віталій");
        People person2 = new People("Андрій");

        aerialReconnaissance.addObserver(person1);
        aerialReconnaissance.addObserver(person2);

        aerialReconnaissance.raiseAlert("Повітряна тривога");


        aerialReconnaissance.clearAlert();

    }
}
