package hw18.observer;

public interface Observer {
    void update(String alert);
}
