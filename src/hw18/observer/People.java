package hw18.observer;

public class People implements Observer{
    private String name;
    private boolean alarm = true;

    public People(String name) {
        this.name = name;
    }

    @Override
    public void update(String alert) {
        System.out.println(alert);
        if (alarm) {
            goToShelter();
        }

    }
    private void goToShelter() {
        System.out.println(name + " негайно пройдіть в укриття!");
        alarm=false;
    }
}

