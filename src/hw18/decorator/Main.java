package hw18.decorator;

public class Main {
    public static void main(String[] args) {

        Drone drone = new KamikazeDroneDecorator(
                        new CombatDroneDecorator(
                          new ReconnaissanceDroneDecorator(
                             new BasicDrone()
                        )
                      )
                    );

        System.out.println(drone.performTask());




    }
}
