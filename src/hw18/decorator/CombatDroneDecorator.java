package hw18.decorator;

public class CombatDroneDecorator extends DroneDecorator{


    public CombatDroneDecorator(Drone drone) {
        super(drone);
    }

    public String combatSkill() {
        return " Дрон теперь вміє атакувати ціль.";
    }

    @Override
    public String performTask() {
        return super.performTask()+combatSkill();
    }
}
