package hw18.decorator;

public class KamikazeDroneDecorator extends DroneDecorator{


    public KamikazeDroneDecorator(Drone drone) {
        super(drone);
    }
    public String kamikazeSkill(){
          return " Дрон теперь вміє самознищюватись.";
    }

    @Override
    public String performTask() {
        return super.performTask() + kamikazeSkill();
    }
}
