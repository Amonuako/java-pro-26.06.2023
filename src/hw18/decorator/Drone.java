package hw18.decorator;

public interface Drone {
     String performTask();
}
