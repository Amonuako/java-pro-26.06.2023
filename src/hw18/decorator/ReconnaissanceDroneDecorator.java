package hw18.decorator;

public class ReconnaissanceDroneDecorator extends DroneDecorator{


    public ReconnaissanceDroneDecorator(Drone drone) {
        super(drone);
    }



    public String reconnaissance(){
       return " Дрон тепер вміє розвідувати місцевість.";
    }

    @Override
    public String performTask() {
        return super.performTask()+reconnaissance();
    }
}
