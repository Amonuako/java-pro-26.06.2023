package hw17;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class  AbstractLogger implements Logger{
    protected final LoggerConfiguration config;
    protected File logFile;
    protected long currentSize;
    protected int logCount = 1;

    public AbstractLogger(LoggerConfiguration config) {
        this.config = config;
        this.logFile = new File(config.getFilePath());
        this.currentSize = logFile.length();
    }

    protected void createNewLogFile() {
        String timestamp = getCurrentTimestamp();
        String newFileName = "Log_" + timestamp + "-" + logCount + ".txt";
        logFile = new File(newFileName);
        logCount++;
        currentSize = 0;
    }

    protected String getCurrentTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy-HH:mm");
        return dateFormat.format(new Date());
    }

    protected void checkFileSize(long messageSize) throws FileMaxSizeReachedException {
        if (currentSize + messageSize > config.getMaxSize()) {
            createNewLogFile();
        }
    }

    @Override
    public abstract void debug(String message) throws IOException;

    @Override
    public abstract void info(String message) throws IOException;
}

