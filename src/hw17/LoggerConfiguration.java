package hw17;

public abstract class LoggerConfiguration {
    private String filePath;
    private LoggingLevel logLevel;
    private long maxSize;
    private LoggingFormat logFormat;

    public LoggerConfiguration() {
    }

    public LoggerConfiguration(String filePath, LoggingLevel logLevel, long maxSize, LoggingFormat logFormat) {
        this.filePath = filePath;
        this.logLevel = logLevel;
        this.maxSize = maxSize;
        this.logFormat = logFormat;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public LoggingLevel getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LoggingLevel logLevel) {
        this.logLevel = logLevel;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }

    public LoggingFormat getLogFormat() {
        return logFormat;
    }

    public void setLogFormat(LoggingFormat logFormat) {
        this.logFormat = logFormat;
    }
}
