package hw17;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileLoggerConfigurationLoader {
    public static FileLoggerConfiguration load(String filePath) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            FileLoggerConfiguration config = new FileLoggerConfiguration();
            String line;

            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(": ");
                if (parts.length == 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();

                    switch (key) {
                        case "FILE" -> config.setFilePath(value);
                        case "LEVEL" -> {
                            try {
                                config.setLogLevel(LoggingLevel.valueOf(value));
                            } catch (IllegalArgumentException e) {
                                throw new IllegalArgumentException("Invalid log level. Please specify one of: INFO, DEBUG");
                            }
                        }
                        case "MAX-SIZE" -> config.setMaxSize(Long.parseLong(value));
                        case "FORMAT" -> {
                            try {
                                config.setLogFormat(LoggingFormat.valueOf(value));
                            } catch (IllegalArgumentException e) {
                                throw new IllegalArgumentException("Invalid log format. Please specify one of: JSON, CSV , XML");
                            }
                        }
                    }
                }
            }

            return config;
        }
    }


}
