package hw17;

import java.io.FileOutputStream;
import java.io.IOException;


public class FileLogger extends AbstractLogger {
    public FileLogger(LoggerConfiguration config) {
        super(config);
    }

    @Override
    public void debug(String message) throws IOException {
        if (config.getLogLevel() == LoggingLevel.DEBUG) {
            String logMessage = "[" + getCurrentTimestamp() + "][DEBUG] " + message + System.lineSeparator();
            byte[] messageBytes = logMessage.getBytes();
            checkFileSize(messageBytes.length);

            try (FileOutputStream fos = new FileOutputStream(logFile, true)) {
                fos.write(messageBytes);
            }
            currentSize += messageBytes.length;
        }
    }

    @Override
    public void info(String message) throws IOException {
        if (config.getLogLevel() == LoggingLevel.DEBUG || config.getLogLevel() == LoggingLevel.INFO) {
            String logMessage = "[" + getCurrentTimestamp() + "][INFO] " + message + System.lineSeparator();
            byte[] messageBytes = logMessage.getBytes();
            checkFileSize(messageBytes.length);

            try (FileOutputStream fos = new FileOutputStream(logFile, true)) {
                fos.write(messageBytes);
            }
            currentSize += messageBytes.length;
        }
    }
}














