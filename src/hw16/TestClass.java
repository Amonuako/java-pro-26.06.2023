package hw16;

public class TestClass {

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("beforeSuite");
    }
    @Test
    public void test(){
        System.out.println("test");
    }
    @Test(priority = 1)
    public  void test1(){
        System.out.println("test1");
    }
    @Test(priority = 2)
    public  void test2(){
        System.out.println("test2");
    }
    @Test(priority = 3)
    public  void test3(){
        System.out.println("test3");

    }
    @AfterSuite
    public void afterSuite(){
        System.out.println("afterSuite");

    }
}
