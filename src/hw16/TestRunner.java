package hw16;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestRunner {
    public static void start(Class<?> testCLass){
    try {
        Object instance = testCLass.getDeclaredConstructor().newInstance();
        Method[] methods = testCLass.getDeclaredMethods();

        List<Method> beforeSuiteMethods = new ArrayList<>();
        List<Method> testMethods = new ArrayList<>();
        List<Method> afterSuiteMethods = new ArrayList<>();

        for (Method method : methods) {
            if (method.isAnnotationPresent(BeforeSuite.class)) {
                beforeSuiteMethods.add(method);
            } else if (method.isAnnotationPresent(Test.class)) {
                testMethods.add(method);
            } else if (method.isAnnotationPresent(AfterSuite.class)) {
                afterSuiteMethods.add(method);
            }
        }

        if (beforeSuiteMethods.size() > 1 || afterSuiteMethods.size() > 1) {
            throw new RuntimeException("Only one @BeforeSuite and one @AfterSuite method allowed.");
        }

        testMethods.sort(Comparator.comparingInt(method -> method.getAnnotation(Test.class).priority()));

        if (!beforeSuiteMethods.isEmpty()) {
            beforeSuiteMethods.get(0).invoke(instance);
        }
        for (Method testMethod : testMethods) {
            testMethod.invoke(instance);
        }

        if (!afterSuiteMethods.isEmpty()) {
            afterSuiteMethods.get(0).invoke(instance);
        }
    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
    InvocationTargetException e) {
        e.printStackTrace();
    }
}
    }

