public abstract class Animals {
    public String name;
    protected static  int maxRun ;
    protected   int maxSwim ;
    protected static int animalCount = 0;


    public Animals(String name,int maxRun,int maxSwim) {
        this.name = name;
        this.maxRun = maxRun;
        this.maxSwim = maxSwim;

        animalCount++;
    }
    public void run(int distance) {
        if (distance <= maxRun) {
            System.out.println(name + " пробежал " + distance + " м");;
        } else {
            System.out.println(name + " не может пробежать " + distance + " м");
        }
    }

    public void swim(int distance) {
        if (distance <= maxSwim) {
            System.out.println(name + " проплыл " + distance + " м");
        } else {
            System.out.println(name + " не может проплыть " + distance + " м");
        }
    }
    public static int getAnimalCount() {
        return animalCount;
    }
}
